# demo-no-train

## How to use

### Reset

0. remove pipelines
1. @k33g: empty `animals.txt` and `mushrooms.txt`
2. @k33g: deactivate "mushrooms linter"
  ```yaml
  script:
    - echo "👋 hello world 🌍"
    - echo "[]" > gl-code-quality-report.json
    #- node emoji-linter.js <mushrooms>
  ```
3. commit on master / show code quality panel in the pipeline

### 3 Merge Requests

1. @k33g: activate the linter and create a MR (**MR1**)
  ```yaml
  script:
    - echo "👋 hello world 🌍"
    #- echo "[]" > gl-code-quality-report.json
    - node emoji-linter.js <mushrooms>
  ```
2. @theroro: add <mushrooms> to `mushrooms.txt` and create a MR (**MR2**)
3. @swannou: add 🐱🐰 to `animals.txt` and create a MR (**MR3**)
4. Merge **MR1**, then merge **MR2**, then merge **MR3**
5. ✋ show pipelines

