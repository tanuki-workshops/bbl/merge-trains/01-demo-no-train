#!/usr/bin/env node

const fs = require("fs")
const { exec } = require("child_process")

const severityValues = ["info", "minor", "major", "critical", "blocker"]

/* --- helpers --- */
let btoa = (string) => Buffer.from(string).toString("base64")
let fingerprint = (description, path, line) => `${btoa(description)}-${btoa(path)}-${btoa(line)}`

let cmdArgs = process.argv.slice(2);
let pattern = cmdArgs[0] || ""
let reportName = cmdArgs[1] || "./report.txt" 

// 🔎 search for ${pattern}
// 🖐️ don't search it the yaml files
let cmd = `grep -wni ${pattern} $(find . ! -path '*/\.*' ! -name "*.yml" ! -name ".") > ${reportName}`

exec(cmd, (error, stdout, stderr) => {

  if (error) {
    // the grep command didn't find pattern(s) in the source code 
    // 🖐️ a code error is generated if grep does not get results
    console.log("🟢", `the grep command didn't find ${pattern} in the source code `)
    // 📝 generate an empty report
    fs.writeFileSync("./gl-code-quality-report.json", "[]")
    //process.exit(0)
  } else { 
    // the grep command found pattern(s) in the source code
    
    try {
      // 📕 read the file and transform it
      let hotSpots = fs.readFileSync(reportName).toString()
      
      console.log("🔴", `the grep command found ${pattern} in the source code `)
      console.log("📝", hotSpots)

      // 📝 create the report
      let report = hotSpots .split("\n").map(item => item.split(":")).filter(item => item.length > 1)
        .map(item => {
          return {
            description: `🖐️: ${item[2].trim()} found!`,
            fingerprint: fingerprint(item[2], item[0], item[1]),
            location: {
              path: item[0],
              lines: {
                begin: parseInt(item[1],10)
              }
            },
            severity: severityValues[3]                  
          }
        })
      
      console.log("🖐️📝", report)
      fs.writeFileSync("./gl-code-quality-report.json", JSON.stringify(report, null, 2))
      process.exit(1)

    } catch(error) {
      console.log(`😡📝 file error(s) found: ${error.message}`)
    }
    
  }

  if (stderr) {
    console.log(`😡 stderr: ${stderr}`)
    return
  }

})




